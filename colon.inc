;colon "third word", third_word
;db "third word explanation", 0
;
; пример
;x1:
;dq x2 
;dq 100

; определим link на следующий ((ранее добавленый элемент) )
%define link 0 

;%1 = Ключ (в кавычках)
;%2 = Имя метки, по которой будет находиться значение.

%macro colon 2
    %2: ;текущий маркер  (x1:)
    dq link   ; линкуем на сл (dq x2)
    db %1, 0    ; сохр. парамеров
    %define link %2
%endmacro